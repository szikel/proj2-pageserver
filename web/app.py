from flask import Flask, render_template, request, abort

app = Flask(__name__)

@app.route("/")
@app.route("/<path:path>")
def hello(path=None):
    if path is None:
        abort(404)
    elif path[0] is "/" or "~" in path or "//" in path or ".." in path:
        abort(403)

    elif path.endswith('.css'):
        try: 
            with open("static/" + path, "r") as f:
                content = f.read()
            return "<pre>" + content + "</pre>"
        except Exception as e:
            abort(404) 
    else:
        try: 
            return render_template(path)
        except Exception as e:
            abort(404)

@app.errorhandler(404)
def error_404(error):
    return render_template("404.html"),404
    
@app.errorhandler(403)
def error_403(error):
    return render_template("403.html"),403

# debug = true will enable auto-reload
# when there are code changes... cool!
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
